import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from "react-navigation-stack";
import {persistor, store} from './redux/store';
import {connect, Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import DiscoverScreen from './screens/discover_screen';
import SettingsScreen from './screens/settings_screen';
import HistoryScreen from './screens/history_screen';

const Root = createStackNavigator({
    DiscoverScreen: {screen: DiscoverScreen},
    SettingsScreen: {screen: SettingsScreen},
    HistoryScreen: {screen: HistoryScreen}
}, {
    initialRouteName: 'DiscoverScreen',
    headerMode: 'none'
});

const AppContainer = createAppContainer(Root);

console.disableYellowBox = false;

class App extends React.Component {
    render() {
        return (
            <Provider store={store}>
                <PersistGate loading={null} persistor={persistor}>
                    <AppContainer/>
                </PersistGate>
            </Provider>
        );
    }
}

export default App;
