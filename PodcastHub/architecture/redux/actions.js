import {createAction} from "redux-actions";
import {getData} from '../api';

export const getAudioData = createAction('send/getAudioData', () => {
    return getData({}, 'all_audio')
});

export const setData = createAction('nav/set', (data) => data);
