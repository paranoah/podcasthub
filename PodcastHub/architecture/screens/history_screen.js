import React from 'react';
import {Image, View, FlatList, TouchableOpacity, StyleSheet, SafeAreaView, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import Item from '../components/DiscoverListItem';
import {getAudioData} from '../redux/actions';
import {connect} from 'react-redux';
import Header from '../components/Heder';
import {undefinedObject} from '../api/external_functions';

const mapStateToProps = state => ({
    author: undefinedObject('Author.Name', state.currentPodcast, 'NoName Author'),
    title: undefinedObject('Title', state.currentPodcast, 'NoName Podcast')
});

const mapDispatchToProps = dispatch => ({
    //getAudioData: () => dispatch(getAudioData()),
});

class HistoryScreen extends React.Component {
    componentDidMount() {
        //this.props.getAudioData();
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <LinearGradient colors={['#100E2D', '#17143D']} style={{flex: 1}}>
                    <View style={{paddingLeft: 25, paddingRight: 25, paddingBottom: 25}}>
                        <Text style={{color: 'white'}}>{this.props.title}</Text>
                    </View>
                </LinearGradient>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({});

export default connect(mapStateToProps, mapDispatchToProps)(HistoryScreen);
