import React from 'react';
import {Image, View, FlatList, TouchableOpacity, StyleSheet, SafeAreaView, Text} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/FontAwesome';
import Item from '../components/DiscoverListItem';
import {getAudioData} from '../redux/actions';
import {connect} from 'react-redux';
import Header from '../components/Heder';
import {undefinedObject} from '../api/external_functions';

const mapStateToProps = state => ({});

const mapDispatchToProps = dispatch => ({});

class SettingsScreen extends React.Component {
    componentDidMount() {
        //this.props.getAudioData();
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <LinearGradient colors={['#100E2D', '#17143D']} style={{flex: 1}}>
                    <View style={{paddingLeft: 25, paddingRight: 25, paddingBottom: 25}}>
                        <Header leftButton={
                            <TouchableOpacity style={{width: 20, height: 21.5}}
                                              onPress={() => this.props.navigation.navigate('DiscoverScreen')}>
                                <Icon name='chevron-left' color='white' size={20}/>
                            </TouchableOpacity>}
                                title={'Settings'}/>
                        <View style={{marginTop: 25}}>
                            <View style={{borderBottomWidth: 1, borderBottomColor: '#2E2C48'}}>
                                <TouchableOpacity style={styles.cart}>
                                    <Text style={styles.text}>Terms & Conditions</Text>
                                    <Icon name='chevron-right' color='#ffffff80' size={15}/>
                                </TouchableOpacity>
                            </View>
                            <View style={{borderBottomWidth: 1, borderBottomColor: '#2E2C48'}}>
                                <TouchableOpacity style={styles.cart}>
                                    <Text style={styles.text}>Feedback</Text>
                                    <Icon name='chevron-right' color='#ffffff80' size={15}/>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </LinearGradient>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({
    cart: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        width: '100%',
        height: 70,
        alignItems: 'center',
    },
    text: {
        color: 'white',
        fontSize: 20,
        fontFamily: 'SofiaProRegular',
        //fontStyle: 'italic',
        fontWeight: 'normal',
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(SettingsScreen);
