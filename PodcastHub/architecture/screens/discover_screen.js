import React from 'react';
import {Image, View, FlatList, TouchableOpacity, StyleSheet, SafeAreaView} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Icon from 'react-native-vector-icons/Ionicons';
import Item from '../components/DiscoverListItem';
import {getAudioData, setData} from '../redux/actions';
import {connect} from 'react-redux';
import Header from '../components/Heder';
import {undefinedObject} from '../api/external_functions';

const mapStateToProps = state => ({
    data: state.data,
});

const mapDispatchToProps = dispatch => ({
    getAudioData: () => dispatch(getAudioData()),
    setData: (data) => dispatch(setData(data))
});

class DiscoverScreen extends React.Component {
    componentDidMount() {
        this.props.getAudioData();
    }

    render() {
        return (
            <SafeAreaView style={{flex: 1}}>
                <LinearGradient colors={['#100E2D', '#17143D']} style={{flex: 1}}>
                    <View style={{paddingLeft: 25, paddingRight: 25, paddingBottom: 25}}>
                        <Header rightButton={
                            <TouchableOpacity style={{width: 20, height: 21.5}}
                                              onPress={() => this.props.navigation.navigate('SettingsScreen')}>
                                <Image source={require('../constants/images/settings.png')}
                                       style={{width: 20, height: 21.5}}/>
                            </TouchableOpacity>}
                                title={'Discover'}/>
                        <View>
                            <FlatList
                                data={this.props.data}
                                renderItem={({item, index}) => <Item title={undefinedObject('Title', item, 'NoName Podcast')}
                                                                     imageUri={{
                                                                         uri: undefinedObject(
                                                                             'Image.Src',
                                                                             item,
                                                                             'https://facebook.github.io/react-native/img/tiny_logo.png',
                                                                         ),
                                                                     }}
                                                                     action={async () => {
                                                                         await this.props.setData({currentPodcast: item});
                                                                         this.props.navigation.navigate('HistoryScreen')
                                                                     }}
                                                                     key={index}
                                                                     author={undefinedObject('Author.Name', item, 'NoName Author')}
                                                                     rating={undefinedObject('Rating', item, 0)}/>}
                                contentContainerStyle={{paddingBottom: 80}}
                                showsVerticalScrollIndicator={false}
                            />
                        </View>
                    </View>
                </LinearGradient>
            </SafeAreaView>
        );
    }
}

const styles = StyleSheet.create({});

export default connect(mapStateToProps, mapDispatchToProps)(DiscoverScreen);
