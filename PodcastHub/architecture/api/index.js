import {mergeRight} from "ramda";
import axios from './instance'

export const getData = async (data, url) => {
    return new Promise(async (res, rej) => {
        try {
            let result;
            switch (url) {
                case 'all_audio':
                    result = await axios.get('/audios', {});
                    break;
                default:
            }
            if (result.data.result)
                res(result.data);
            else
                rej(result)
        } catch (e) {
            rej(e)
        }
    })
};

export const sendData = (data, token, url) => {
    return new Promise(async (res, rej) => {
        try {
            let result;
            switch (url) {
                default:
            }
            if (result.data.success)
                res(result.data);
            else
                rej(result)
        } catch (e) {
            rej(e)
        }
    })
}
