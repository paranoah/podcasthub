import {path} from 'ramda'

export const undefinedObject = (stringPath, object, defaultResult = undefined) => {
    return path(stringPath.split('.').join('[').split('[').join(']').split(']').filter(e => !!e), object) || defaultResult
};

/*
let a = {b: {c : {d: 3}}};
((((a || {}).b || {}).c || {}).e || {}).d

undefinedObject('b.c.d', a, 0) => 3
undefinedObject('b.c.e.d', a, 0) => 0
*/
