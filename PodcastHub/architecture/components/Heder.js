import React from 'react';
import {StyleSheet, Text, View} from 'react-native';

function Header({title, rightButton, leftButton}) {
    return (
        <View style={styles.container}>
            <View style={styles.buttonRow}>
                {leftButton}
                {rightButton}
            </View>
            <Text style={styles.headerTitle}>
                {title}
            </Text>
        </View>
    )
}

Header.defaultProps = {
    title: '',
    rightButton: <View/>,
    leftButton: <View/>
};

const styles = StyleSheet.create({
    container: {
        height: 116,
        width: '100%'
    },
    headerTitle: {
        fontFamily: 'SofiaProLight',
        color: '#fff',
        fontSize: 38,
        lineHeight: 48,
        marginBottom: 15,
    },
    buttonRow: {
        justifyContent: 'space-between',
        width: '100%',
        flexDirection: 'row',
        height: 48,
        alignItems: 'flex-end',
    }
});

export default Header
