import {Image, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import React from 'react';
import {Rating} from 'react-native-ratings';

function Item({title, imageUri, author, rating, action}) {
    let starRating = rating / 10;
    return (
        <TouchableOpacity style={styles.podcastBlock} onPress={action}>
            <View style={styles.podcastImage}>
                <Image width={140} height={100} source={imageUri} style={styles.image}/>
            </View>
            <View style={{width: '55%', flex: 1, flexDirection: 'column', alignContent: 'space-between'}}>
                <View style={styles.podcastTextBlock}>
                    <Text numberOfLines={1} ellipsizeMode={'tail'} style={styles.podcastTitle}>{title}</Text>
                    <Text numberOfLines={1} style={styles.podcastAutor}>{author}</Text>
                </View>
                <View style={styles.podcastRatingBlock}>
                    <View style={styles.podcastRatingItems}>
                        <Rating
                            //style={{tintColor: '#00000000'}}
                            //tintColor='#FFA723'
                            type='custom'
                            fractions={20}
                            imageSize={20}
                            readonly
                            ratingBackgroundColor={'#00000000'}
                            starContainerStyle={{backgroundColor: 'red'}}
                            ratingImage={require('../constants/images/star_blue.png')}
                            ratingCount={1}
                            ratingColor='#FFA723'
                            startingValue={starRating}
                        />
                        <Text style={styles.ratingText}>{rating.toFixed(1)}</Text>
                    </View>
                </View>
            </View>
        </TouchableOpacity>
    );
}

Item.defaultProps = {
    title: '',
    imageUri: '',
    author: '',
    rating: 0,
    action: () => {}
};

const styles = StyleSheet.create({
    podcastBlock: {
        flex: 1,
        flexDirection: 'row',
        marginVertical: 15,
    },
    podcastTitle: {
        color: '#fff',
        fontFamily: 'SofiaProRegular',
        fontSize: 20,
        lineHeight: 30,
        fontWeight: '400',
        marginBottom: 10,
    },
    podcastImage: {
        width: 140,
        height: 100,
        marginRight: 25,
    },
    podcastAutor: {
        fontSize: 13,
        lineHeight: 22,
        color: '#CCC9DB',
        fontFamily: 'SofiaProRegular',
        opacity: 0.7,
    },
    podcastTextBlock: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-start',
    },
    podcastRatingBlock: {
        flex: 1,
        flexDirection: 'column',
        justifyContent: 'flex-end',
    },
    ratingText: {
        fontWeight: '500',
        fontSize: 13,
        lineHeight: 20,
        color: '#FFA723',
        marginLeft: 10,
    },
    podcastRatingItems: {
        flex: 1,
        flexDirection: 'row',
    },
    image: {
        width: 140,
        height: 100,
        borderRadius: 20,
    },
});

export default Item;
