import {AppRegistry} from 'react-native';
import App from './architecture/App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
